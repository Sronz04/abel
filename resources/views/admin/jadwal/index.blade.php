@extends('admin.layouts.main')

@section('title', 'Manajemen Jadwal')

@section('headTitle', 'Jadwal Pelajaran')

@section('content')

<div class="container">
	<div class="row">

		<div class="col-md-4">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <label class="input-group-text" for="inputGroupSelect01">Hari</label>
			  </div>
			  <select class="custom-select" id="inputGroupSelect01">
			    <option selected>Selasa</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <label class="input-group-text" for="inputGroupSelect01">Kelas</label>
			  </div>
			  <select class="custom-select" id="inputGroupSelect01">
			    <option selected>XII-RPL-1</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
			</div>
		</div>

		<div class="col-md-4">
			<div class="input-group mb-3">
			  <div class="input-group-append">
			    <label class="input-group-text" for="inputGroupSelect02">Jadwal</label>
			  </div>
			  <select class="custom-select" id="inputGroupSelect02">
			    <option selected>Jadwal KBM</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col">
				<div class="btn-group btn-group-toggle">
				  <a href="/schedule/create" class="text-white btn btn-primary rounded-left"> Add</a>
				  <a href="/schedule/edit" class="text-white btn btn-warning rounded-right"> Edit</a>
				</div>
		</div>
	</div>

	<div class="row justify-content-center mt-3">
		<div class="col col-md">
			<table class="table table-bordered table-light justify-content-center table-hover" style="text-align: center;">
			  <thead>
			    <tr class="bg-primary">
			      <th scope="col">#</th>
			      <th scope="col">Waktu</th>
			      <th scope="col">Mata Pelajaran</th>
			      <th scope="col">Kode Guru</th>
			      <th scope="col">Kode Ruang</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">1</th>
			      <td>07:00 - 07:45</td>
			      <td>Matematika</td>
			      <td>En</td>
			      <td>10</td>
			    </tr>
			    <tr>
			      <th scope="row">2</th>
			      <td>07:45 - 08:30</td>
			      <td>Matematika</td>
			      <td>En</td>
			      <td>10</td>
			    </tr>
			    <tr>
			      <th scope="row">3</th>
			      <td>08:30 - 09:15</td>
			      <td>Matematika</td>
			      <td>En</td>
			      <td>10</td>
			    </tr>
			    <tr>
			      <th scope="row">4</th>
			      <td>09:15 - 10:00</td>
			      <td>Matematika</td>
			      <td>En</td>
			      <td>10</td>
			    </tr>
			    <tr>
			      <th scope="row">5</th>
			      <td>10:00 - 10:30</td>
			      <td colspan="3">Rehat</td>
			    </tr>
			    <tr>
			      <th scope="row">6</th>
			      <td>10:30 - 11:15</td>
			      <td>PKK</td>
			      <td>Af</td>
			      <td>01</td>
			    </tr>
			    <tr>
			      <th scope="row">7</th>
			      <td>11:15 - 12:00</td>
			      <td>PKK</td>
			      <td>Af</td>
			      <td>01</td>
			    </tr>
			    <tr>
			      <th scope="row">8</th>
			      <td>12:00 - 12:15</td>
			      <td colspan="3">Sholat Dzuhur Berjamaah</td>
			    </tr>
			    <tr>
			      <th scope="row">9</th>
			      <td>12:15 - 13:00</td>
			      <td>Bahasa Jepang</td>
			      <td>Vn</td>
			      <td>20</td>
			    </tr>
			    <tr>
			      <th scope="row">10</th>
			      <td>13:00 - 13:45</td>
			      <td>Bahasa Jepang</td>
			      <td>Vn</td>
			      <td>20</td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</div>
</div>

@endsection