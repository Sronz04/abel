@extends('admin.layouts.main')

@section('title', 'Manajemen Jadwal')

@section('headTitle', 'Tambah Jadwal Pelajaran')

@section('content')

<div class="container">
	<div class="row">

		<div class="col-md-4">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <label class="input-group-text" for="inputGroupSelect01">Hari</label>
			  </div>
			  <select class="custom-select" id="inputGroupSelect01">
			    <option selected>Selasa</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <label class="input-group-text" for="inputGroupSelect01">Kelas</label>
			  </div>
			  <select class="custom-select" id="inputGroupSelect01">
			    <option selected>XII-RPL-1</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
			</div>
		</div>

		<div class="col-md-4">
			<div class="input-group mb-3">
			  <div class="input-group-append">
			    <label class="input-group-text" for="inputGroupSelect02">Jadwal</label>
			  </div>
			  <select class="custom-select" id="inputGroupSelect02">
			    <option selected>Jadwal KBM</option>
			    <option value="1">One</option>
			    <option value="2">Two</option>
			    <option value="3">Three</option>
			  </select>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col">
			<button type="button" class="btn btn-outline-success rounded">Tambah Baris</button>
		</div>
	</div>

	<div class="row justify-content-center mt-3">
		<div class="col col-md">
			<table class="table table-bordered table-light justify-content-center table-hover table-responsive-sm" style="text-align: center;">
			  <thead>
			    <tr class="bg-primary">
			      <th scope="col">#</th>
			      <th scope="col">Waktu</th>
			      <th scope="col">Mata Pelajaran</th>
			      <th scope="col">Kode Guru</th>
			      <th scope="col">Kode Ruang</th>
			      <th scope="col">Tools</th>
			    </tr>
			  </thead>
			  <tbody>

			    <tr>
			      <th scope="row">1</th>
			      <td>
			      	  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="07:00">
					  </div>
					  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="07:45">
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Matematika</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Enni Soesilaningsih, SPd, MM</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>10</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	<button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
			      </td>
			    </tr>

			    <tr>
			      <th scope="row">2</th>
			      <td>
			      	  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="07:45">
					  </div>
					  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="08:30">
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	<button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
			      </td>
			    </tr>

			    <tr>
			      <th scope="row">3</th>
			      <td>
			      	  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="08:30">
					  </div>
					  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="09:15">
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	<button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
			      </td>
			    </tr>

			    <tr>
			      <th scope="row">4</th>
			      <td>
			      	  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="09:15">
					  </div>
					  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="10:00">
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	<button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
			      </td>
			    </tr>

			    <tr>
			      <th scope="row">5</th>
			      <td>
			      	  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="10:30">
					  </div>
					  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="11:15">
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	<button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
			      </td>
			    </tr>

			    <tr>
			      <th scope="row">6</th>
			      <td>
			      	  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="11:15">
					  </div>
					  <div class="form-group col col-md-6">
					    <input type="time" class="form-control" value="12:00">
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose...</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	  <div class="form-group">
					    <select class="form-control" id="exampleFormControlSelect1">
					      <option selected>Choose</option>
					    </select>
					  </div>
			      </td>
			      <td>
			      	<button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
			      </td>
			    </tr>

			  </tbody>
			</table>
		</div>
	</div>
</div>

@endsection