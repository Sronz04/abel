@extends('admin.layouts.main')

@section('title', 'Dashboard')

@section('headTitle', 'Dashboard')

@section('content')

            <div class="container">
                <div class="row">                        

                	<div class="col-sm-6 col-lg-3">
		                <div class="card text-white bg-flat-color-1">
		                    <div class="card-body pb-0">
		                        <div class="dropdown float-right">
		                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
		                                <i class="fa fa-user"></i>
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
		                                <div class="dropdown-menu-content">
		                                    <a class="dropdown-item" href="#">Action</a>
		                                </div>
		                            </div>
		                        </div>
		                        <h4 class="mb-0">
		                            <span class="count">1868</span>
		                        </h4>
		                        <p class="text-light">Siswa</p>

		                    </div>

		                </div>
		            </div>

		            <div class="col-sm-6 col-lg-3">
		                <div class="card text-white bg-flat-color-2">
		                    <div class="card-body pb-0">
		                        <div class="dropdown float-right">
		                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
		                                <i class="fa fa-user"></i>
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
		                                <div class="dropdown-menu-content">
		                                    <a class="dropdown-item" href="#">Action</a>
		                                </div>
		                            </div>
		                        </div>
		                        <h4 class="mb-0">
		                            <span class="count">287</span>
		                        </h4>
		                        <p class="text-light">Guru</p>

		                    </div>

		                </div>
		            </div>

		            <div class="col-sm-6 col-lg-3">
		                <div class="card text-white bg-flat-color-3">
		                    <div class="card-body pb-0">
		                        <div class="dropdown float-right">
		                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
		                                <i class="fa fa-calendar"></i>
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
		                                <div class="dropdown-menu-content">
		                                    <a class="dropdown-item" href="#">Action</a>
		                                </div>
		                            </div>
		                        </div>
		                        <h4 class="mb-0">
		                            <span class="">{{ date('Y-m-d') }}</span>
		                        </h4>
		                        <p class="text-light">Manage Jadwal</p>

		                    </div>

		                </div>
		            </div>

		            <div class="col-sm-6 col-lg-3">
		                <div class="card text-white bg-flat-color-4">
		                    <div class="card-body pb-0">
		                        <div class="dropdown float-right">
		                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
		                                <i class="fa fa-school"></i>
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
		                                <div class="dropdown-menu-content">
		                                    <a class="dropdown-item" href="#">Action</a>
		                                </div>
		                            </div>
		                        </div>
		                        <h4 class="mb-0">
		                            <span class="count">32</span> - <span class="count">54</span>
		                        </h4>
		                        <p class="text-light">Ruang dan Kelas</p>

		                    </div>

		                </div>
		            </div>

		            <div class="col-sm-6 col-lg-3">
		                <div class="card text-white bg-flat-color-5">
		                    <div class="card-body pb-0">
		                        <div class="dropdown float-right">
		                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
		                                <i class="fa fa-user-shield"></i>
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
		                                <div class="dropdown-menu-content">
		                                    <a class="dropdown-item" href="#">Action</a>
		                                </div>
		                            </div>
		                        </div>
		                        <h4 class="mb-0">
		                            <span class="count">11</span>
		                        </h4>
		                        <p class="text-light">Kepengurusan</p>

		                    </div>

		                </div>
		            </div>

                </div>
            </div>

@endsection