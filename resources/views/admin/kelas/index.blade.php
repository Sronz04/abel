@extends('admin.layouts.main')

@section('title', 'Manajemen Sekolah')

@section('headTitle', 'Ruang dan Kelas')

@section('content')

<div class="container">
	
	<div class="row">
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">KELAS : XII-TKJ-I</h5>
	        <p class="card-text">Teknik Komputer dan Jaringan. <br> Teknik Informatika dan Komputer</p>
	        <a href="#" class="btn btn-primary rounded">Go </a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">KELAS : XII-TKJ-II</h5>
	        <p class="card-text">Teknik Komputer dan Jaringan. <br> Teknik Informatika dan Komputer</p>
	        <a href="#" class="btn btn-primary rounded">Go </a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">KELAS : XII-RPL-I</h5>
	        <p class="card-text">Rekayasa Perangkat Lunak. <br> Teknik Informatika dan Komputer</p>
	        <a href="#" class="btn btn-primary rounded">Go </a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">KELAS : XII-RPL-II</h5>
	        <p class="card-text">Rekayasa Perangkat Lunak. <br> Teknik Informatika dan Komputer</p>
	        <a href="#" class="btn btn-primary rounded">Go </a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">KELAS : XII-MM-I</h5>
	        <p class="card-text">Multimedia. <br> Teknik Informatika dan Komputer</p>
	        <a href="#" class="btn btn-primary rounded">Go </a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">KELAS : XII-MM-II</h5>
	        <p class="card-text">Multimedia. <br> Teknik Informatika dan Komputer</p>
	        <a href="#" class="btn btn-primary rounded">Go </a>
	      </div>
	    </div>
	  </div>

	</div>

</div>

@endsection