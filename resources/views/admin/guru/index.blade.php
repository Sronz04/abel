@extends('admin.layouts.main')

@section('title', 'All Teacher')

@section('headTitle', 'All Teacher')

@section('content')

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <div class="dropdown-menu-content">
                                    <a class="dropdown-item" href="/teacher/create">Create New</a>
                                </div>
                            </div>
                        </div>
                        
                        <h4 class="mb-0">
                            <span class="count">{{ count($teachers) }}</span>
                        </h4>
                        <p class="text-light">Teacher</p>

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="container">
                <div class="row">
                    <div class="col-xl">
                        <table id="table-teacher" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIP</th>
                                <th scope="col">Nama</th>
                                <!-- <th scope="col">Guru Bidang</th> -->
                                <th scope="col">TTL</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Action</th>                         
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teachers as $teacher)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $teacher->NIP }}</td>
                                <td>{{ $teacher->nama }}</td>
                                <td>{{ $teacher->tanggal_lahir . ', ' . $teacher->tempat_lahir }}</td>
                                <td>{{ $teacher->alamat }}</td>
                                <td>
                                    <div class="btn-group btn-group-toggle">
                                      <a href="/student/edit/{{ $teacher->id }}" class="text-white btn btn-warning rounded-right"> Edit</a>
                                      <form action="student/{{ $teacher->id }}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger mx-1">Delete</button>  
                                      </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection