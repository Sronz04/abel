@extends('admin.layouts.main')

@section('title', 'All Student')

@section('headTitle', 'All Student')

@section('content')

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <div class="dropdown-menu-content">
                                    <a class="dropdown-item" href="/student/create">Create New</a>
                                </div>
                            </div>
                        </div>
                        
                        <h4 class="mb-0">
                            <span class="count">{{ count($students) }}</span>
                        </h4>
                        <p class="text-light">Student</p>

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="container">
                <div class="row">
                    <div class="col-xl">
                    <table id="table-student" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIS</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Kelas</th>
                                <th scope="col">TTL</th>
                                <th scope="col">Alamat</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $student)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $student->nis }}</td>
                                <td>{{ $student->nama }}</td>
                                <td>{{ $student->kelas_id = 1 ? "XII-RPL-I" : "XII-RPL-II" }}</td>
                                <td>{{ $student->tempat_lahir . ', ' . $student->tanggal_lahir }}</td>
                                <td>{{ $student->alamat }}</td>
                                <td>
                                    <div class="btn-group btn-group-toggle">
                                      <a href="/student/edit/{{ $student->id }}" class="text-white btn btn-warning rounded-right"> Edit</a>
                                      <form action="student/{{ $student->id }}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger mx-1">Delete</button>  
                                      </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection