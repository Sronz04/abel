@extends('admin.layouts.main')

@section('title', 'Create New Data Student')

@section('headTitle', 'Student')

@section('content')

            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                    <form method="POST" action="/student/update/{{$students->id}}">

                        @csrf
                        @method('patch')

                        
                        <div class="form-group">
                            <label for="nis">NIS</label>
                            <input type="number" max-length="9" class="form-control" id="nis" name="nis" placeholder="Enter NIS" value="{{ $students->nis}}">
                        </div>
                        <div class="form-group">
                            <label>Nama Student</label>
                            <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Siswa" value="{{$students->nama}}">

                        </div>
                        <div class="form-group">
                            <label for="identified">Identified</label>
                            <input type="text" class="form-control" id="identified" name="identified" placeholder="Enter Identified" value=" {{$students->identified}}">
                        </div>
                        <div class="form-group">
                            <label for="kelas_id">Kelas</label>
                            <select class="form-control" id="kelas_id" name="kelas_id">
                            @foreach($kelas as $k)
                                <option 

                                    @if($k->id == $students->kelas_id)
                                        $selected = "selected"
                                    @endif

                                    value="{{ $k->id }}"

                                    >{{ $k->nama_kelas }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Enter Birth Date" value="">
                        </div>
                        <div class="form-group">
                            <label for="tempat_lahir">Tempat Lahir</label>
                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Enter Birth Place" value=" {{$students->tempat_lahir}}">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" name="alamat" rows="3" value="">{{$students->alamat}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    </div>
                </div>
            </div>
@endsection