@extends('admin.layouts.main')

@section('title', 'Create New Data Student')

@section('headTitle', 'Student')

@section('content')

            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                    <form method="POST" action="/student/store_student">
                    @csrf

                        <div class="form-group">
                            <label for="nis">NIS</label>
                            <input type="number" max-length="9" class="form-control" id="nis" name="nis" placeholder="Enter NIS">
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter Name">
                        </div>
                        <div class="form-group">
                            <label for="identified">Identified</label>
                            <input type="text" class="form-control" id="identified" name="identified" placeholder="Enter Identified">
                        </div>
                        <div class="form-group">
                            <label for="kelas_id">Kelas</label>
                            <select class="form-control" id="kelas_id" name="kelas_id">
                                <option value="">--Choose--</option>
                            @foreach($kelas as $k)
                                <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Enter Birth Date">
                        </div>
                        <div class="form-group">
                            <label for="tempat_lahir">Tempat Lahir</label>
                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Enter Birth Place">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    </div>
                </div>
            </div>
@endsection