<?php

namespace App\Http\Controllers;

use App\Student;
use App\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::with('class')->get();
        return view('admin.siswa.index', [
            'students' => $students,
        ]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Classes::all();


        return view('admin.siswa.create', [
            'kelas' => $kelas,
        ]);
        // return response()->json($data['kelas']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nis' => 'required',
            'nama' => 'required',
            'identified' => 'required',
            'kelas_id' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_lahir' => 'required',
            'alamat' => 'required',
        ]);

        Student::create([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'identified' => $request->identified,
            'kelas_id' => $request->kelas_id,
            'tanggal_lahir' => $request->tanggal_lahir,
            'tempat_lahir' => $request->tempat_lahir,
            'alamat' => $request->alamat,
        ]);

        return redirect("/student");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $students)
    {
        $kelas = Classes::all();
        // $siswa = DB::table('student')->where('nis',$nis)->get();
        
        // return view('admin.siswa.edit',[
        //     'kelas' => $kelas,
        //     'nis' => $nis
        // ]);
        

        return view('admin.siswa.edit', ['students' => $students, 'kelas' => $kelas]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    
    
   public function update(Student $student ,Request $request )
    {

        Student::where('id', $student->id)
            ->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'identified' => $request->identified,
                // 'kelas_id' => $request->kelas,
                'tanggal_lahir' => $request->tanggal_lahir,
                'tempat_lahir' => $request->tempat_lahir,
                'alamat' => $request->alamat
            ]);
            return redirect('/student')->with('status', 'Data Berhasil Diubah!');
    }
 


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        Student::destroy($student->id);
        return redirect('/student')->with('status', 'Berhasil Dihapus!');
    }
}
