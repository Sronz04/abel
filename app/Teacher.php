<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
Use App;

class teacher extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teachers';

    protected $primarykey = 'id';
    
    protected $fillable = [
        'NIP',
        'nama',
        'tanggal_lahir',
        'tempat_lahir',
        'alamat'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'identified',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function roomteacher()
    {
        return $this->belongsTo('App\RoomTeacher', 'id_guru', 'id');
    }

    public function majorteacher()
    {
        return $this->belongsTo('App\MajorTeacher', 'id_guru', 'id');
    }

    public function homreroomtecher()
    {
        return $this->belongsTo('App\HomeroomTeahcer', 'id_guru','id');
    }
    public function courseteacher()
    {
        return $this->belongsTo('App\CourseTeacher', 'id_guru', 'id');
    }

}
