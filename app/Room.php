<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rooms';

    protected $primarykey = 'id';
    
    protected $fillable = [
        'nomor_ruang',
        'nama_ruang',
        'jenis_ruang',
        'deskripsi',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'identified',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function roomtype()
    {
        return $this->belongsTo('App\RoomType', 'id', 'jenis_ruang');
    }

    public function roomteacher()
    {
    	return $this->belongsTo('App\RoomTeacher', 'id_ruangan','id');
    }
}
