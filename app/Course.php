<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'courses';

    protected $primarykey = 'id';
    
    protected $fillable = [
        'kode_mapel',
        'nama_mapel',
        'jenis_mapel',
        'deskripsi',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function courseteacher()
    {
        return $this->belongsTo('App\CourseTeacher', 'id_mapel', 'id');
    }

    public function coursetype()
    {
    	return $this->belongsTo('App\CourseType', 'id_mapel', 'id');
    }

    public function courseclasses()
    {
    	return $this->belongsTo('App\CourseClass', 'id', 'id_pelajaran');
    }
}
