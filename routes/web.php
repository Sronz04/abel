<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/student', 'StudentController@index');
Route::get('/student/create', 'StudentController@create');
Route::post('/student/store_student', 'StudentController@store');
Route::get('/student/edit/{students}', 'StudentController@edit');
Route::delete('/student/{student}', 'StudentController@destroy');
Route::patch('/student/update/{student}', 'StudentController@update');

Route::get('/teacher', 'TeacherController@index');
Route::get('/teacher/create', 'TeacherController@create');
Route::post('/teacher/store_teacher', 'TeacherController@store');


Route::get('/schedule', 'ScheduleController@index');
Route::get('/schedule/create', 'ScheduleController@create');
Route::get('/schedule/edit', 'ScheduleController@edit');

Route::get('/class', 'ClassController@index');
Route::get('/class/create', 'ClassController@create');
Route::get('/class/edit', 'ClassController@edit');
