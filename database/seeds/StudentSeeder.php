<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\Student;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 40; $i++) {
        	$name = $faker->firstName;
        	$dataStudent = new Student;
            $dataStudent->nis = strval(171810000 + $i);
            $dataStudent->nama = $name;
            $dataStudent->identified = $name;
            $dataStudent->kelas_id = $faker->randomElement($array = array('1', '2', '3'));
            $dataStudent->tanggal_lahir = $faker->date($format = 'Y-m-d', $max = '2003-12-30', $min = "2001-01-01");
            $dataStudent->tempat_lahir = $faker->city;
            $dataStudent->alamat = $faker->address;
            $dataStudent->save();
        }

    }
}
